package com.app.currency.network;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.app.currency.App;
import com.app.currency.interfaces.NetworkCallback;
import com.app.currency.model.Currency;
import com.app.currency.util.DebugLogger;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.OkHttpClient;
import retrofit2.Response;

public class NetworkHelper {

    private Handler handler = new Handler(Looper.getMainLooper());


    public void getTodayCurrencyExchange(@Nullable final NetworkCallback<List<Currency>> networkCallback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String message = null;
                int code = 0;
                try {
                    Response<List<Currency>> response = App.getApp().getCurrencyApi().getTodayExchange().execute();
                    if (response.isSuccessful()) {
                        final List<Currency> currencyList = response.body();
                        if(currencyList!=null){
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if(networkCallback!=null){
                                        networkCallback.onSuccess(currencyList);
                                    }
                                }
                            });
                            return;
                        }
                    }
                    code = response.code();
                    message = response.message();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                final String finalMessage = message;
                final int finalCode = code;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(networkCallback!=null){
                            networkCallback.onError(finalMessage, finalCode);
                        }
                    }
                });
           /*     HttpUrl.Builder urlBuilder = HttpUrl.parse("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json").newBuilder();
                String url = urlBuilder.build().toString();
                Request request = new Request.Builder()
                        .url(url)
                        .build();
                int code = -1;
                String message = null;
                try {
                    Response response = client.newCall(request).execute();
                    String responseText = response.message();
                    message = responseText;
                    code = response.code();
                    ResponseBody responseBody = response.body();
                    if (responseBody != null) {
                        responseText = responseBody.string();
                    }
                    final Currency[] currencies = new Gson().fromJson(responseText, Currency[].class);
                    DebugLogger.e(NetworkHelper.class.getSimpleName(), "parsed: " + currencies.length);
                    DebugLogger.e(NetworkHelper.class.getSimpleName(), "RESPONSE: " + responseText);
                    new Handler(mainLooper).post(new Runnable() {
                        @Override
                        public void run() {
                            if (networkCallback != null) {
                                networkCallback.onSuccess(currencies);
                            }
                        }
                    });
                } catch (IOException e) {
                    DebugLogger.e(NetworkHelper.class.getSimpleName(), "getTodayCurrencyExchange", e);
                    final int finalCode = code;
                    final String finalMessage = message;
                    new Handler(mainLooper).post(new Runnable() {
                        @Override
                        public void run() {
                            if (networkCallback != null) {
                                networkCallback.onError(finalMessage, finalCode);
                            }
                        }
                    });
                }*/
            }
        }).start();

    }

    public void getCurrencyExchangeByDate(@NonNull final Date date, @Nullable final NetworkCallback<List<Currency>> networkCallback){
        new Thread(new Runnable() {
            @Override
            public void run() {
                String message = null;
                int code = 0;
                try {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
                    String dateValue = simpleDateFormat.format(date);
                    DebugLogger.e("getCurrencyExchangeByDate for date", " "+dateValue);
                    Response<List<Currency>> response = App.getApp().getCurrencyApi().getExchange(dateValue).execute();
                    if (response.isSuccessful()) {
                        final List<Currency> currencyList = response.body();
                        if(currencyList!=null){
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if(networkCallback!=null){
                                        networkCallback.onSuccess(currencyList);
                                    }
                                }
                            });
                            return;
                        }
                    }
                    code = response.code();
                    message = response.message();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                final String finalMessage = message;
                final int finalCode = code;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(networkCallback!=null){
                            networkCallback.onError(finalMessage, finalCode);
                        }
                    }
                });
            }
        }).start();
    }

}
