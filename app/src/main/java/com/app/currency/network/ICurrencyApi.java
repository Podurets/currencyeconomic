package com.app.currency.network;

import android.support.annotation.NonNull;

import com.app.currency.model.Currency;

import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ICurrencyApi {

    @GET("statdirectory/exchange?json")
    Call<List<Currency>> getTodayExchange();

    @GET("statdirectory/exchange?json")
    Call<List<Currency>> getExchange(@Query("date") @NonNull String date);
}
