package com.app.currency.interfaces;

public interface PositionClickListener {

    void onClick(int position);
}
