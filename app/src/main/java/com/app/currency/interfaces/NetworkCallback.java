package com.app.currency.interfaces;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public interface NetworkCallback<Data> {

    void onSuccess(@NonNull Data data);

    void onError(@Nullable String message, int code);
}
