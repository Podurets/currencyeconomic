package com.app.currency.interfaces;

public interface ListItemClickListener<Data> {

    void onClick(Data data);
}
