package com.app.currency.interfaces;

import android.support.annotation.NonNull;

import com.app.currency.model.NoteEvent;

public interface NotesEventsAdapterClickListener {

    void onClick(int position, @NonNull NoteEvent event);
    void onDelete(int position, @NonNull NoteEvent event);
    void onEdit(int position, @NonNull NoteEvent event);

}
