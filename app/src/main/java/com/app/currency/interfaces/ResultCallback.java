package com.app.currency.interfaces;

import android.support.annotation.Nullable;

public interface ResultCallback<Result> {

    void onResult(@Nullable Result result);

}
