package com.app.currency.modules.edit_event;

import com.app.currency.modules.common.IManageEventModel;

public interface IEditEventModel extends IManageEventModel<IEditEventViewModel.MA> {
    void loadEvent(int editEventId);
    void loadEvent();
    void requestUpdateByEvent();
}
