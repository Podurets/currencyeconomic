package com.app.currency.modules.recordslist;

import android.support.annotation.NonNull;

import com.app.currency.common.IBaseMA;
import com.app.currency.model.NoteEvent;

public interface IRecordsListModel extends IBaseMA<IRecordsListPresenter.MA>  {

    void loadNoteEvents();
    void deleteNote(@NonNull NoteEvent noteEvent);

}
