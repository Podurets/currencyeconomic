package com.app.currency.modules.createevent;

import com.app.currency.common.IBasePA;
import com.app.currency.modules.common.IManageEventViewModel;

public interface ICreateEventViewModel extends IBasePA {

    interface VA extends IManageEventViewModel.VA {

    }

    interface MA extends IManageEventViewModel.MA {

    }


}
