package com.app.currency.modules.hryvnyacalc;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.app.currency.common.IBaseMA;
import com.app.currency.model.Currency;

public interface ICalculateHryvnyaByCurrencyModel extends IBaseMA<ICalculateHryvnyaByCurrencyPresenter.MA>  {

    void notifyCurrency(@NonNull Currency currency);
    @Nullable
    Currency getCurrency();

    double calculateCurrency(double value);

}
