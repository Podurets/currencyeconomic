package com.app.currency.modules.hryvnyacourse;

import android.support.annotation.NonNull;

import com.app.currency.common.BasePresenter;
import com.app.currency.model.Currency;

import java.util.Date;
import java.util.List;

public class CourseHryvnyaPresenter extends BasePresenter<CourseHryvnyaView, ICourseHryvnyaModel>
        implements ICourseHryvnyaPresenter.VA, ICourseHryvnyaPresenter.MA {


    public CourseHryvnyaPresenter(@NonNull ICourseHryvnyaModel iCourseHryvnyaModel) {
        super(iCourseHryvnyaModel);
    }

    @Override
    protected void updateView() {
        if (model() != null) {
            model().loadCurrencies();
        }
    }

    @Override
    public void loadCurrencies() {
        model().loadCurrencies();
    }

    @Override
    public void onDateViewClicked() {
        if (view() != null && model() != null) {
            view().showDateDialog(model().getCurrencyDate());
        }
    }

    @Override
    public void onDateApplied(@NonNull Date date) {
        if (model() != null) {
            model().applyCurrencyDate(date);
            loadCurrencies();
        }
    }

    @Override
    public void notifyRefreshDateView() {
        if (model() != null && view() != null) {
            view().refreshDateInView(model().getFormattedCurrencyDate());

        }
    }

    @Override
    public void onCurrenciesLoaded(@NonNull List<Currency> currencies) {
        view().drawCurrencies(currencies);
    }
}
