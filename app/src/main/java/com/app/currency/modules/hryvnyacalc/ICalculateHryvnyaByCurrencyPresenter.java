package com.app.currency.modules.hryvnyacalc;

import android.support.annotation.NonNull;

import com.app.currency.common.IBasePA;
import com.app.currency.model.Currency;

public interface ICalculateHryvnyaByCurrencyPresenter extends IBasePA {

     interface VA  extends IBasePA.VA{

         void notifyCurrency(@NonNull Currency currency);
         void notifyUpdateViews();
         void onSomeValueInputed(double value);
    }

    interface MA extends IBasePA.MA{

    }

}
