package com.app.currency.modules.hryvnyacourse;

import android.support.annotation.NonNull;

import com.app.currency.common.IBasePA;
import com.app.currency.model.Currency;

import java.util.Date;
import java.util.List;

public interface ICourseHryvnyaPresenter extends IBasePA {

    interface VA extends IBasePA.VA {

        void loadCurrencies();
        void onDateViewClicked();
        void onDateApplied(@NonNull Date date);
        void notifyRefreshDateView();

    }

    interface MA extends IBasePA.MA {

        void onCurrenciesLoaded(@NonNull List<Currency> currencies);

    }

}
