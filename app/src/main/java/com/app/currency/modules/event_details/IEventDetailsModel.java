package com.app.currency.modules.event_details;

import android.support.annotation.NonNull;

import com.app.currency.common.IBaseMA;
import com.app.currency.model.NoteEvent;

public interface IEventDetailsModel extends IBaseMA<IEventDetailsViewModel.MA>  {

    void attachNoteEvent(@NonNull NoteEvent event);
    void requestNoteEventInfo();
    int getEventId();
    void deleteNoteEvent();
}
