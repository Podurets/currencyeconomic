package com.app.currency.modules.common;

import android.support.annotation.NonNull;

import com.app.currency.common.IBasePA;

public interface IManageEventViewModel extends IBasePA {

    interface VA extends IBasePA.VA {
        void onEventDataAccepted(@NonNull String name, @NonNull String description);
        void onDateEventClicked();
        void onNotificationDateClicked();
        void onDateEventAccepted(long dateEvent);
        void onNotificationDateAccepted(long notificationDate);
    }

    interface MA extends IBasePA.MA {
        void onEventSaved();
        void drawDateEventText(String text);
        void drawNotificationTimeEventText(String text);
    }

}
