package com.app.currency.modules.event_details;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;

import com.app.currency.common.BasePresenter;
import com.app.currency.db.NoteEventDbHelper;
import com.app.currency.model.NoteEvent;
import com.app.currency.util.Broadcaster;

public class EventDetailsViewModel extends BasePresenter<IEventDetailsView, IEventDetailsModel>
        implements IEventDetailsViewModel.VA, IEventDetailsViewModel.MA {

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (model() != null) {
                int eventId = model().getEventId();
                if (eventId > 0) {
                    NoteEvent noteEvent = new NoteEventDbHelper().get(eventId);
                    if (noteEvent != null) {
                        attachNoteEvent(noteEvent);
                    }
                }
            }
        }
    };

    public EventDetailsViewModel(@NonNull IEventDetailsModel iEventDetailsModel) {
        super(iEventDetailsModel);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Broadcaster.EVENT_NOTE_CREATED_ACTION);
        intentFilter.addAction(Broadcaster.EVENT_NOTE_UPDATED_ACTION);
        intentFilter.addAction(Broadcaster.EVENT_NOTE_DELETED_ACTION);
        Broadcaster.registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void updateView() {
        if (model() != null) {
            model().requestNoteEventInfo();
        }
    }

    @Override
    public void attachNoteEvent(@NonNull NoteEvent event) {
        if (model() != null) {
            model().attachNoteEvent(event);
            model().requestNoteEventInfo();
        }
    }

    @Override
    public void onEditEventClicked() {
        if (model() != null) {
            int eventId = model().getEventId();
            if (eventId > 0) {
                if (view() != null) {
                    view().showEditEventScreen(eventId);
                }
            }
        }
    }

    @Override
    public void onDeleteEventClicked() {
        if (model() != null) {
            model().deleteNoteEvent();
        }
    }

    @Override
    public void drawNoteEventInfo(String noteText, String noteDescription, String noteTimeText, String noteNotificationDateText) {
        if (view() != null) {
            view().drawNoteEventInfo(noteText, noteDescription, noteTimeText, noteNotificationDateText);
        }
    }

    @Override
    public void onNoteDeleted() {
        if (view() != null) {
            view().closeScreenOnEventDeleted();
            Broadcaster.sendBroadcast(Broadcaster.EVENT_NOTE_DELETED_ACTION);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Broadcaster.unregisterReceiver(broadcastReceiver);
    }

}
