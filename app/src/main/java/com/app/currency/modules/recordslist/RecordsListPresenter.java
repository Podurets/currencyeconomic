package com.app.currency.modules.recordslist;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;

import com.app.currency.App;
import com.app.currency.R;
import com.app.currency.common.BasePresenter;
import com.app.currency.model.NoteEvent;
import com.app.currency.util.Broadcaster;

import java.util.List;

public class RecordsListPresenter extends BasePresenter<IRecordsListView, IRecordsListModel>
        implements IRecordsListPresenter.MA, IRecordsListPresenter.VA {

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateView();
        }
    };

    public RecordsListPresenter(@NonNull IRecordsListModel iRecordsListModel) {
        super(iRecordsListModel);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Broadcaster.EVENT_NOTE_CREATED_ACTION);
        intentFilter.addAction(Broadcaster.EVENT_NOTE_UPDATED_ACTION);
        intentFilter.addAction(Broadcaster.EVENT_NOTE_DELETED_ACTION);
        Broadcaster.registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void updateView() {
        loadNoteEvents();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Broadcaster.unregisterReceiver(broadcastReceiver);
    }

    private void loadNoteEvents() {
        if (model() != null) {
            model().loadNoteEvents();
        }
    }

    @Override
    public void onRecordsLoaded(@NonNull List<NoteEvent> result) {
         if(view()!=null){
             view().drawRecords(result);
         }
    }

    @Override
    public void onNoteDeleted(@NonNull NoteEvent noteEvent) {
        if (view() != null) {
            view().onNoteDeleted(noteEvent);
        }
    }

    @Override
    public void onErrorDeleteNote(@NonNull NoteEvent noteEvent) {
        onError(App.getApp().getString(R.string.cannot_delete) + " " + noteEvent.getNoteText());
    }

    @Override
    public void onAddItemClicked() {
        if (view() != null) {
            view().openCreateEventDialog();
        }
    }

    @Override
    public void notifyUpdateData() {
        loadNoteEvents();
    }

    @Override
    public void onDeleteNoteEvent(@NonNull NoteEvent noteEvent) {
        if (view() != null) {
            view().showDeleteNoteDialog(noteEvent);
        }
    }

    @Override
    public void onEditNoteEvent(@NonNull NoteEvent noteEvent) {
        if (view() != null) {
            view().startEditNotePage(noteEvent);
        }
    }

    @Override
    public void onClickNoteEvent(@NonNull NoteEvent noteEvent) {
        if (view() != null) {
            view().startDetailsNotePage(noteEvent);
        }
    }

    @Override
    public void onDeleteNoteEventConfirmed(@NonNull NoteEvent noteEvent) {
        if (model() != null) {
            model().deleteNote(noteEvent);
        }
    }
}
