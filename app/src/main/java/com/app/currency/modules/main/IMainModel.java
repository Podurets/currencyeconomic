package com.app.currency.modules.main;

import com.app.currency.common.IBaseMA;

public interface IMainModel extends IBaseMA<IMainPresenter.MA>  {
}
