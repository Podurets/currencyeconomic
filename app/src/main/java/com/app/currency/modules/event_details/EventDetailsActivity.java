package com.app.currency.modules.event_details;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.app.currency.App;
import com.app.currency.R;
import com.app.currency.common.PresenterActivity;
import com.app.currency.model.NoteEvent;
import com.app.currency.modules.edit_event.EditEventActivity;
import com.app.currency.util.AppConstants;

public class EventDetailsActivity extends PresenterActivity<IEventDetailsViewModel.VA> implements IEventDetailsView {

    private TextView titleTv;
    private TextView descriptionTv;
    private TextView dateValueTv;
    private TextView notificationDateTv;

    @NonNull
    @Override
    public IEventDetailsViewModel.VA createPresenter() {
        return new EventDetailsViewModel(new EventDetailsModel());
    }

    public static Intent buildLaunchIntent(@NonNull Context context, @NonNull NoteEvent noteEvent) {
        Intent intent = new Intent(context, EventDetailsActivity.class);
        intent.putExtra(AppConstants.INTENT_DATA_KEY, noteEvent);
        return intent;
    }

    @Nullable
    public static NoteEvent retrieveEvent(@NonNull Intent intent) {
        return intent.getParcelableExtra(AppConstants.INTENT_DATA_KEY);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_details);
        titleTv = findViewById(R.id.tv_title_value);
        descriptionTv = findViewById(R.id.tv_description_value);
        dateValueTv = findViewById(R.id.tv_date_value);
        notificationDateTv = findViewById(R.id.tv_notification_value);
        NoteEvent noteEvent = retrieveEvent(getIntent());
        if (noteEvent == null) {
            finish();
            return;
        }
        if (getPresenter() != null) {
            getPresenter().attachNoteEvent(noteEvent);
        }
        bindView(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_delete_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit:
                if (getPresenter() != null) {
                    getPresenter().onEditEventClicked();
                }
                return true;
            case R.id.delete:
                if (getPresenter() != null) {
                    getPresenter().onDeleteEventClicked();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void drawNoteEventInfo(String noteText, String noteDescription, String noteTimeText, String noteNotificationDateText) {
        titleTv.setText(noteText);
        descriptionTv.setText(noteDescription);
        dateValueTv.setText(TextUtils.isEmpty(noteTimeText) ? App.getApp().getText(R.string.date_absent) : noteTimeText);
        notificationDateTv.setText(TextUtils.isEmpty(noteNotificationDateText) ? App.getApp().getText(R.string.missing_notification) : noteNotificationDateText);
    }

    @Override
    public void showEditEventScreen(int eventId) {
       startActivity(EditEventActivity.buildIntent(this, eventId));
    }

    @Override
    public void closeScreenOnEventDeleted() {
        showToast(getString(R.string.deleted));
        finish();
    }
}
