package com.app.currency.modules.common;

import android.support.annotation.NonNull;

import com.app.currency.common.BasePresenter;

public abstract class ManageEventViewModel<ICreateEventView extends IManageEventView, ICreateEventModel extends IManageEventModel>
        extends BasePresenter<ICreateEventView, ICreateEventModel>
        implements IManageEventViewModel.MA, IManageEventViewModel.VA {

    public ManageEventViewModel(@NonNull ICreateEventModel iCreateEventModel) {
        super(iCreateEventModel);
    }

    @Override
    public void onEventDataAccepted(@NonNull String name, @NonNull String description) {
        if (model() != null) {
            model().saveNotificationEvent(name, description);
        }
    }

    @Override
    public void onDateEventClicked() {
        if (view() != null && model() != null) {
            view().showDateDialog(model().getDateEvent());
        }
    }

    @Override
    public void onNotificationDateClicked() {
        if (view() != null && model() != null) {
            view().showNotificationDateDialog(model().getNotificationDate());
        }
    }

    @Override
    public void onDateEventAccepted(long dateEvent) {
        if (model() != null) {
            model().acceptDateEvent(dateEvent);
        }
    }

    @Override
    public void onNotificationDateAccepted(long notificationDate) {
        if (model() != null) {
            model().acceptNotificationDate(notificationDate);
        }
    }

    @Override
    public void drawDateEventText(String text) {
        if (view() != null) {
            view().setDateText(text);
        }
    }

    @Override
    public void drawNotificationTimeEventText(String text) {
        if (view() != null) {
            view().setNotificationDateText(text);
        }
    }

}
