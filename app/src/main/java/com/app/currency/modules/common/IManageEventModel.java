package com.app.currency.modules.common;

import com.app.currency.common.IBaseMA;

public interface IManageEventModel<ViewModel extends IManageEventViewModel.MA> extends IBaseMA<ViewModel> {
    long getDateEvent();
    long getNotificationDate();

    void acceptDateEvent(long dateTime);
    void acceptNotificationDate(long dateTime);

    void saveNotificationEvent(String name, String description);
}
