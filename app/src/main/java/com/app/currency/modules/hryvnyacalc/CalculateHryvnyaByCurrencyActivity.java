package com.app.currency.modules.hryvnyacalc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import com.app.currency.R;
import com.app.currency.common.PresenterActivity;
import com.app.currency.model.Currency;
import com.app.currency.util.AppConstants;
import com.app.currency.util.DebugLogger;

public class CalculateHryvnyaByCurrencyActivity extends PresenterActivity<ICalculateHryvnyaByCurrencyPresenter.VA> implements ICalculateHryvnyaByCurrencyView {

    private TextView currencyNameTextView;
    private EditText valueInputEditText;
    private TextView resultTextView;

    public static Intent buildIntent(@NonNull Activity activity, @NonNull Currency currency) {
        Intent intent = new Intent(activity, CalculateHryvnyaByCurrencyActivity.class);
        intent.putExtra(AppConstants.INTENT_DATA_KEY, currency);
        return intent;
    }

    @Nullable
    public Currency obtainFromIntent(@NonNull Intent intent) {
        return intent.getParcelableExtra(AppConstants.INTENT_DATA_KEY);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculate_hryvnya_by_currency);
        Currency currency = obtainFromIntent(getIntent());
        if (currency == null) {
            finish();
            return;
        }
        currencyNameTextView = findViewById(R.id.currency_name_tv);
        valueInputEditText = findViewById(R.id.value_input_et);
        resultTextView = findViewById(R.id.result_tv);
        valueInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s == null) {
                    processInputedCurrencyValue(0);
                } else {
                    String text = s.toString();
                    if (TextUtils.isEmpty(text)) {
                        processInputedCurrencyValue(0);
                    } else {
                        try {
                            Double number = Double.valueOf(text);
                            processInputedCurrencyValue(number);
                        } catch (Exception e) {
                            DebugLogger.e(CalculateHryvnyaByCurrencyActivity.class.getSimpleName(),
                                    "afterTextChanged", e);
                        }
                    }
                }
            }
        });
        getPresenter().bindView(this);
        getPresenter().notifyCurrency(currency);
        getPresenter().notifyUpdateViews();
    }

    private void processInputedCurrencyValue(double value) {
        if (getPresenter() != null) {
            getPresenter().onSomeValueInputed(value);
        }
    }

    @NonNull
    @Override
    public ICalculateHryvnyaByCurrencyPresenter.VA createPresenter() {
        return new CalculateHryvnyaByCurrencyPresenter(new CalculateHryvnyaByCurrencyModel());
    }

    @Override
    public void updateCurrencyName(@NonNull String name) {
        currencyNameTextView.setText(name);
    }

    @Override
    public void updateCalculatedConvertedResult(@Nullable String result) {
        resultTextView.setText(result);
    }
}
