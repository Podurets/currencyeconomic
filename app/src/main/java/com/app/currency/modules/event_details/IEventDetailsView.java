package com.app.currency.modules.event_details;

import com.app.currency.common.IBaseVA;

public interface IEventDetailsView extends IBaseVA {
    void drawNoteEventInfo(String noteText, String noteDescription, String noteTimeText, String noteNotificationDateText);
    void showEditEventScreen(int eventId);
    void closeScreenOnEventDeleted();
}
