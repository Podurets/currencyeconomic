package com.app.currency.modules.edit_event;

import android.support.annotation.NonNull;

import com.app.currency.common.BasePresenter;
import com.app.currency.modules.common.ManageEventModel;
import com.app.currency.modules.common.ManageEventViewModel;
import com.app.currency.util.Broadcaster;

public class EditEventViewModel extends ManageEventViewModel<IEditEventView, IEditEventModel> implements
        IEditEventViewModel.VA, IEditEventViewModel.MA {

    public EditEventViewModel(@NonNull IEditEventModel iEditEventModel) {
        super(iEditEventModel);
    }

    @Override
    protected void updateView() {
        if (model() != null) {
            model().loadEvent();
        }
    }

    @Override
    public void onErrorLoadRecord() {
        if (view() != null) {
            view().onErrorLoadRecord();
        }
    }

    @Override
    public void setDateText(String text) {
        if (view() != null) {
            view().setDateText(text);
        }
    }

    @Override
    public void setTitleText(String text) {
        if (view() != null) {
            view().setTitleText(text);
        }
    }

    @Override
    public void setDescriptionText(String text) {
        if (view() != null) {
            view().setDescriptionText(text);
        }
    }

    @Override
    public void setNotificationDateText(String text) {
        if (view() != null) {
            view().setNotificationDateText(text);
        }
    }


    @Override
    public void onEventSaved() {
        if (view() != null) {
            view().onSuccessSavedEvent();
            Broadcaster.sendBroadcast(Broadcaster.EVENT_NOTE_UPDATED_ACTION);
        }
    }

}
