package com.app.currency.modules.edit_event;

import com.app.currency.modules.common.IManageEventViewModel;

public interface IEditEventViewModel extends IManageEventViewModel {

    interface MA extends IManageEventViewModel.MA {
         void onErrorLoadRecord();
        void setDateText(String text);
        void setTitleText(String text);
        void setDescriptionText(String text);
        void setNotificationDateText(String text);
    }

    interface VA extends IManageEventViewModel.VA {

    }

}
