package com.app.currency.modules.hryvnyacourse;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.app.currency.common.BaseModel;
import com.app.currency.interfaces.NetworkCallback;
import com.app.currency.model.Currency;
import com.app.currency.network.NetworkHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CourseHryvnyaModel extends BaseModel<ICourseHryvnyaPresenter.MA> implements ICourseHryvnyaModel {

    @NonNull
    private Date currencyDate = Calendar.getInstance().getTime();
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());

    @Override
    public void loadCurrencies() {
        new NetworkHelper().getCurrencyExchangeByDate(currencyDate, new NetworkCallback<List<Currency>>() {
            @Override
            public void onSuccess(@NonNull List<Currency> currencies) {
               // new CurrencyDbHelper().saveCurrenciesAsync(currencies);
                presenter().onCurrenciesLoaded(currencies);
            }

            @Override
            public void onError(@Nullable String message, int code) {
                presenter().onError(message);
            }
        });
       /* new Thread(new Runnable() {
            @Override
            public void run() {
                List<Currency> currencyList = new CurrencyDbHelper().getCurrencies("22.01.2018");
                DebugLogger.e(MainActivity.class.getSimpleName(), "Count loaded: " + currencyList.size());
            }
        }).start();
        presenter().onCurrenciesLoaded(new ArrayList<Currency>());*/
    }

    @NonNull
    @Override
    public Date getCurrencyDate() {
        return currencyDate;
    }

    @Override
    public void applyCurrencyDate(@NonNull Date date) {
        currencyDate = date;
    }

    @Override
    public String getFormattedCurrencyDate() {
        return dateFormat.format(getCurrencyDate());
    }
}
