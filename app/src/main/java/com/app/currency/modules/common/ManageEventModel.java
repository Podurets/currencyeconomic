package com.app.currency.modules.common;

import android.support.annotation.NonNull;

import com.app.currency.App;
import com.app.currency.R;
import com.app.currency.common.BaseModel;
import com.app.currency.util.SettingsConfigurator;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public abstract class ManageEventModel<ViewModel extends IManageEventViewModel.MA> extends BaseModel<ViewModel> implements IManageEventModel<ViewModel> {

    protected final Calendar calendar = Calendar.getInstance();

    protected SimpleDateFormat dateFormat = SettingsConfigurator.getSettingsConfigurator().getDefaultSimpleDateFormat();

    protected boolean checkNotificationDate(long notificationTime) {
        if (notificationTime <= Calendar.getInstance().getTimeInMillis()) {
            if (presenter() != null) {
                presenter().onError(App.getApp().getString(R.string.past_notification_date_message));
            }
            return false;
        }
        if (notificationTime > getOriginEventTime() && getOriginEventTime() != 0) {
            if (presenter() != null) {
                presenter().onError(App.getApp().getString(R.string.notification_date_less_event_date_message));
            }
            return false;
        }
        return true;
    }

    public abstract long getOriginEventTime();

}
