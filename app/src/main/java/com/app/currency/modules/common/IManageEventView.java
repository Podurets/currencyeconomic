package com.app.currency.modules.common;

import com.app.currency.common.IBaseVA;

public interface IManageEventView extends IBaseVA {
    void setDateText(String text);
    void setNotificationDateText(String text);
    void showNotificationDateDialog(long dateTime);
    void showDateDialog(long dateTime);
    void onSuccessSavedEvent();
}
