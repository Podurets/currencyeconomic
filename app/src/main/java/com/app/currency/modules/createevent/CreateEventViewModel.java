package com.app.currency.modules.createevent;

import android.support.annotation.NonNull;

import com.app.currency.common.BasePresenter;
import com.app.currency.modules.common.ManageEventViewModel;
import com.app.currency.util.Broadcaster;

public class CreateEventViewModel extends ManageEventViewModel<ICreateEventView, ICreateEventModel>
        implements ICreateEventViewModel.MA, ICreateEventViewModel.VA {

    public CreateEventViewModel(@NonNull ICreateEventModel iCreateEventModel) {
        super(iCreateEventModel);
    }

    @Override
    protected void updateView() {

    }


    @Override
    public void onEventSaved() {
        if (view() != null) {
            view().onSuccessSavedEvent();
            Broadcaster.sendBroadcast(Broadcaster.EVENT_NOTE_CREATED_ACTION);
        }
    }

}
