package com.app.currency.modules.edit_event;

import com.app.currency.modules.common.IManageEventView;

public interface IEditEventView extends IManageEventView {

    void setTitleText(String text);
    void setDescriptionText(String text);
    void onErrorLoadRecord();
}
