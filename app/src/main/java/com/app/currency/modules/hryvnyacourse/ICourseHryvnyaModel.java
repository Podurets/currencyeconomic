package com.app.currency.modules.hryvnyacourse;

import android.support.annotation.NonNull;

import com.app.currency.common.IBaseMA;

import java.util.Date;

public interface ICourseHryvnyaModel extends IBaseMA<ICourseHryvnyaPresenter.MA>  {

    void loadCurrencies();
    @NonNull
    Date getCurrencyDate();
    void applyCurrencyDate(@NonNull Date date);
    String getFormattedCurrencyDate();

}
