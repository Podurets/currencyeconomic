package com.app.currency.modules.edit_event;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.app.currency.App;
import com.app.currency.R;
import com.app.currency.common.BaseModel;
import com.app.currency.db.NoteEventDbHelper;
import com.app.currency.interfaces.ResultCallback;
import com.app.currency.model.NoteEvent;
import com.app.currency.modules.common.ManageEventModel;
import com.app.currency.util.SettingsConfigurator;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EditEventModel extends ManageEventModel<IEditEventViewModel.MA> implements IEditEventModel {

    @Nullable
    private NoteEvent event;
    private SimpleDateFormat dateFormat = SettingsConfigurator.getSettingsConfigurator().getDefaultSimpleDateFormat();
    private int eventId;

    public EditEventModel(int eventId) {
        this.eventId = eventId;
    }

    @Override
    public void loadEvent(int editEventId) {
        this.eventId = editEventId;
        loadEvent();
    }

    @Override
    public void loadEvent() {
        new NoteEventDbHelper().get(eventId, new ResultCallback<NoteEvent>() {
            @Override
            public void onResult(@Nullable NoteEvent event) {
                applyEvent(event);
            }
        });
    }

    @Override
    public void requestUpdateByEvent() {
        IEditEventViewModel.MA ma = presenter();
        if (ma == null || event == null) {
            return;
        }
        ma.setTitleText(event.getNoteText());
        ma.setDescriptionText(event.getDescriptionText());
        if (event.getNoteDate() > 0) {
            ma.setDateText(dateFormat.format(new Date(event.getNoteDate())));
            if (event.getNotificationDate() > 0) {
                ma.setNotificationDateText(dateFormat.format(new Date(event.getNotificationDate())));
            } else {
                ma.setNotificationDateText(App.getApp().getString(R.string.date_absent));
            }
        } else {
            ma.setDateText(App.getApp().getString(R.string.date_absent));
            ma.setNotificationDateText(App.getApp().getString(R.string.date_absent));
        }
    }

    private void applyEvent(@Nullable NoteEvent event) {
        if (event == null) {
            if (presenter() != null) {
                presenter().onErrorLoadRecord();
            }
            return;
        }
        this.event = event;
        requestUpdateByEvent();
    }

    @Override
    public long getOriginEventTime() {
        if (event != null) {
            return event.getNoteDate();
        }
        return 0;
    }

    @Override
    public long getDateEvent() {
        if (event != null) {
           return   event.getNoteDate() > 0 ? event.getNoteDate() : calendar.getTimeInMillis();
        }
        return calendar.getTimeInMillis();
    }

    @Override
    public long getNotificationDate() {
        if (event != null) {
            if (event.getNotificationDate() > 0) {
                return event.getNotificationDate();
            }
        }
        long eventDate = getDateEvent();
        if (eventDate > calendar.getTimeInMillis()) {
            return eventDate;
        }
        return calendar.getTimeInMillis();
    }

    @Override
    public void acceptDateEvent(long dateTime) {
        if (event != null) {
            event.setNoteDate(dateTime);
            if (presenter() != null) {
                presenter().drawDateEventText(dateFormat.format(new Date(dateTime)));
            }
        }
    }

    @Override
    public void acceptNotificationDate(long dateTime) {
        if (event!=null && checkNotificationDate(dateTime)) {
            event.setNotificationDate(dateTime);
            if (presenter() != null) {
                presenter().drawNotificationTimeEventText(dateFormat.format(new Date(dateTime)));
            }
        }
    }

    @Override
    public void saveNotificationEvent(String name, String description) {
        if(event == null){
            return;
        }
        if (TextUtils.isEmpty(name)) {
            if (presenter() != null) {
                presenter().onError(App.getApp().getString(R.string.event_name_required_message));
            }
            return;
        }
        event.setNoteText(name);
        event.setDescriptionText(description);
        if (event.getNotificationDate() > 0) {
            if (!checkNotificationDate(event.getNotificationDate())) {
                return;
            }
        }
        new NoteEventDbHelper().createOrUpdate(event);
        if (presenter() != null) {
            presenter().onEventSaved();
        }
    }
}
