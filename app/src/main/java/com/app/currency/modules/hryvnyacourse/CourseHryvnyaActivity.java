package com.app.currency.modules.hryvnyacourse;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import com.app.currency.R;
import com.app.currency.adapters.CurrencyAdapter;
import com.app.currency.common.PresenterActivity;
import com.app.currency.interfaces.ListItemClickListener;
import com.app.currency.model.Currency;
import com.app.currency.modules.hryvnyacalc.CalculateHryvnyaByCurrencyActivity;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CourseHryvnyaActivity extends PresenterActivity<ICourseHryvnyaPresenter.VA> implements CourseHryvnyaView {

    private RecyclerView recyclerView;
    private TextView dateValueTv;
    private CurrencyAdapter adapter;
    private final Calendar calendar = Calendar.getInstance();
    private DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.course_hryvnya_layout);
        recyclerView = findViewById(R.id.recyclerView);
        dateValueTv = findViewById(R.id.date_value_tv);
        adapter = new CurrencyAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        bindView(this);
        getPresenter().notifyRefreshDateView();
        dateValueTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getPresenter() != null) {
                    getPresenter().onDateViewClicked();
                }
            }
        });
        adapter.setListItemClickListener(new ListItemClickListener<Currency>() {
            @Override
            public void onClick(Currency currency) {
              // startActivity(new Intent(CourseHryvnyaActivity.this, CalculateHryvnyaByCurrencyActivity.class));
               Intent intent = CalculateHryvnyaByCurrencyActivity.buildIntent(CourseHryvnyaActivity.this, currency);
               startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public ICourseHryvnyaPresenter.VA createPresenter() {
        return new CourseHryvnyaPresenter(new CourseHryvnyaModel());
    }

    @Override
    public void drawCurrencies(@NonNull List<Currency> currencies) {
        adapter.setCurrencies(currencies);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showDateDialog(@NonNull Date date) {
        calendar.setTime(date);
        if (datePickerDialog != null) {
            datePickerDialog.dismiss();
        }
        datePickerDialog = new DatePickerDialog(
                this, onDateSetListener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    @Override
    public void refreshDateInView(String dateText) {
        dateValueTv.setText(dateText);
    }

    private DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            if (getPresenter() != null) {
                getPresenter().onDateApplied(calendar.getTime());
                getPresenter().notifyRefreshDateView();
                getPresenter().loadCurrencies();
            }
        }
    };

}
