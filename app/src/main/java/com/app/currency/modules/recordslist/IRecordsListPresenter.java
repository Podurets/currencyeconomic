package com.app.currency.modules.recordslist;

import android.support.annotation.NonNull;

import com.app.currency.common.IBasePA;
import com.app.currency.model.NoteEvent;

import java.util.List;

public interface IRecordsListPresenter extends IBasePA {

    interface VA extends IBasePA.VA {
        void onAddItemClicked();
        void notifyUpdateData();
        void onDeleteNoteEvent(@NonNull NoteEvent noteEvent);
        void onEditNoteEvent(@NonNull NoteEvent noteEvent);
        void onClickNoteEvent(@NonNull NoteEvent noteEvent);
        void onDeleteNoteEventConfirmed(@NonNull NoteEvent noteEvent);
    }

    interface MA extends IBasePA.MA {

        void onRecordsLoaded(@NonNull List<NoteEvent> result);
        void onNoteDeleted(@NonNull NoteEvent noteEvent);
        void onErrorDeleteNote(@NonNull NoteEvent noteEvent);

    }

}
