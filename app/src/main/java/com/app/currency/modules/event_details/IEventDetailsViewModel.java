package com.app.currency.modules.event_details;

import android.support.annotation.NonNull;

import com.app.currency.common.IBasePA;
import com.app.currency.model.NoteEvent;

public interface IEventDetailsViewModel extends IBasePA {

   interface VA extends IBasePA.VA {
       void attachNoteEvent(@NonNull NoteEvent event);
       void onEditEventClicked();
       void onDeleteEventClicked();
   }

   interface MA extends IBasePA.MA {
        void drawNoteEventInfo(String noteText, String noteDescription, String noteTimeText, String noteNotificationDateText);
        void onNoteDeleted();
   }

}
