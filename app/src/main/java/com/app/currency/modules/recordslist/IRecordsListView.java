package com.app.currency.modules.recordslist;

import android.support.annotation.NonNull;

import com.app.currency.common.IBaseVA;
import com.app.currency.model.NoteEvent;

import java.util.List;

public interface IRecordsListView extends IBaseVA {

    void drawRecords(@NonNull List<NoteEvent> result);
    void openCreateEventDialog();
    void startDetailsNotePage(@NonNull NoteEvent noteEvent);
    void startEditNotePage(@NonNull NoteEvent noteEvent);
    void showDeleteNoteDialog(@NonNull NoteEvent noteEvent);
    void onNoteDeleted(@NonNull NoteEvent noteEvent);
}
