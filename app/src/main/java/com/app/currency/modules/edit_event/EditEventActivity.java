package com.app.currency.modules.edit_event;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.app.currency.R;
import com.app.currency.common.PresenterActivity;
import com.app.currency.interfaces.ResultCallback;
import com.app.currency.util.AppConstants;
import com.app.currency.util.DateTimePicker;

import java.util.Date;

public class EditEventActivity extends PresenterActivity<IEditEventViewModel.VA> implements IEditEventView {

    private EditText eventNameEditText;
    private EditText eventDescriptionEditText;
    private Button eventDateButton;
    private Button notificationEventDateButton;

    private int noteEventId;

    private DateTimePicker dateTimePicker = new DateTimePicker();

    @NonNull
    public static Intent buildIntent(@NonNull Context context, int eventId){
        Intent intent = new Intent(context, EditEventActivity.class);
        intent.putExtra(AppConstants.INTENT_DATA_KEY, eventId);
        return intent;
    }

    private static int getEventId(Intent intent) {
        if (intent != null) {
            return intent.getIntExtra(AppConstants.INTENT_DATA_KEY, AppConstants.INVALID_CODE);
        }
        return AppConstants.INVALID_CODE;
    }

    @NonNull
    @Override
    public IEditEventViewModel.VA createPresenter() {
        return new EditEventViewModel(new EditEventModel(noteEventId));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.accept, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accept:
                if (getPresenter() != null) {
                    getPresenter().onEventDataAccepted(eventNameEditText.getText().toString(),
                            eventDescriptionEditText.getText().toString());
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        noteEventId = getEventId(getIntent());
        super.onCreate(savedInstanceState);
        if(noteEventId <= 0){
            finish();
            return;
        }
        setContentView(R.layout.create_event_activity);
        eventNameEditText = findViewById(R.id.title_et);
        eventDescriptionEditText = findViewById(R.id.description_et);
        eventDateButton = findViewById(R.id.event_date_btn);
        eventDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getPresenter() != null) {
                    getPresenter().onDateEventClicked();
                }
            }
        });
        notificationEventDateButton = findViewById(R.id.notification_event_date_btn);
        notificationEventDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getPresenter() != null) {
                    getPresenter().onNotificationDateClicked();
                }
            }
        });
        getPresenter().bindView(this);
    }

    @Override
    public void setDateText(String text) {
        eventDateButton.setText(text);
    }

    @Override
    public void setTitleText(String text) {
        eventNameEditText.setText(text);
    }

    @Override
    public void setDescriptionText(String text) {
        eventDescriptionEditText.setText(text);
    }

    @Override
    public void setNotificationDateText(String text) {
        notificationEventDateButton.setText(text);
    }

    @Override
    public void showNotificationDateDialog(long dateTime) {
        dateTimePicker.setDateResultCallback(new ResultCallback<Date>() {
            @Override
            public void onResult(@Nullable Date date) {
                if (date != null) {
                    if (getPresenter() != null) {
                        getPresenter().onNotificationDateAccepted(date.getTime());
                    }
                }
            }
        });
        dateTimePicker.showDialog(this, dateTime);
    }

    @Override
    public void showDateDialog(long dateTime) {
        dateTimePicker.setDateResultCallback(new ResultCallback<Date>() {
            @Override
            public void onResult(@Nullable Date date) {
                if (date != null) {
                    if (getPresenter() != null) {
                        getPresenter().onDateEventAccepted(date.getTime());
                    }
                }
            }
        });
        dateTimePicker.showDialog(this, dateTime);
    }

    @Override
    public void onSuccessSavedEvent() {
        showToast(getString(R.string.record_saved));
        finish();
    }

    @Override
    public void onErrorLoadRecord() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dateTimePicker.release();
    }
}
