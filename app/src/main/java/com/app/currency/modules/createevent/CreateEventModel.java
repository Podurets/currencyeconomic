package com.app.currency.modules.createevent;

import android.text.TextUtils;

import com.app.currency.App;
import com.app.currency.R;
import com.app.currency.common.BaseModel;
import com.app.currency.db.NoteEventDbHelper;
import com.app.currency.model.NoteEvent;
import com.app.currency.modules.common.ManageEventModel;
import com.app.currency.util.SettingsConfigurator;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CreateEventModel extends ManageEventModel<ICreateEventViewModel.MA> implements ICreateEventModel {

    private long eventTime;
    private long notificationEventTime;

    @Override
    public long getDateEvent() {
        return eventTime > 0 ? eventTime : calendar.getTimeInMillis();
    }

    @Override
    public long getNotificationDate() {
        if (notificationEventTime > 0) {
            return notificationEventTime;
        }
        long eventDate = getDateEvent();
        if (eventDate > calendar.getTimeInMillis()) {
            return eventDate;
        }
        return calendar.getTimeInMillis();
    }

    @Override
    public void acceptDateEvent(long dateTime) {
        eventTime = dateTime;
        if (presenter() != null) {
            presenter().drawDateEventText(dateFormat.format(new Date(eventTime)));
        }
    }

    @Override
    public void acceptNotificationDate(long dateTime) {
        if (checkNotificationDate(dateTime)) {
            notificationEventTime = dateTime;
            if (presenter() != null) {
                presenter().drawNotificationTimeEventText(dateFormat.format(new Date(notificationEventTime)));
            }
        }
    }

    @Override
    public void saveNotificationEvent(String name, String description) {
        if (TextUtils.isEmpty(name)) {
            if (presenter() != null) {
                presenter().onError(App.getApp().getString(R.string.event_name_required_message));
            }
            return;
        }
        NoteEvent noteEvent = new NoteEvent(name);
        noteEvent.setDescriptionText(description);
        if (eventTime > 0) {
            noteEvent.setNoteDate(eventTime);
        }
        if (notificationEventTime > 0) {
            if (!checkNotificationDate(notificationEventTime)) {
                return;
            }
            noteEvent.setNotificationDate(notificationEventTime);
        }
        new NoteEventDbHelper().createOrUpdate(noteEvent);
        if (presenter() != null) {
            presenter().onEventSaved();
        }
    }

    @Override
    public long getOriginEventTime() {
        return eventTime;
    }

}
