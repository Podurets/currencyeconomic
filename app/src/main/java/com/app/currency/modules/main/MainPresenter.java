package com.app.currency.modules.main;

import android.support.annotation.NonNull;

import com.app.currency.common.BasePresenter;

public class MainPresenter extends BasePresenter<MainView, IMainModel> implements IMainPresenter.VA, IMainPresenter.MA{

    public MainPresenter(@NonNull IMainModel iMainModel) {
        super(iMainModel);
    }

    @Override
    protected void updateView() {

    }

}
