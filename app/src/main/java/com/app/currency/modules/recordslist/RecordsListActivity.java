package com.app.currency.modules.recordslist;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.app.currency.R;
import com.app.currency.adapters.NotesEventsAdapter;
import com.app.currency.common.PresenterActivity;
import com.app.currency.interfaces.NotesEventsAdapterClickListener;
import com.app.currency.model.NoteEvent;
import com.app.currency.modules.createevent.CreateEventActivity;
import com.app.currency.modules.edit_event.EditEventActivity;
import com.app.currency.modules.event_details.EventDetailsActivity;
import com.app.currency.util.AppDialogBuilder;
import com.app.currency.util.Broadcaster;

import java.util.List;

public class RecordsListActivity extends PresenterActivity<IRecordsListPresenter.VA> implements IRecordsListView {

    private RecyclerView recyclerView;

    private NotesEventsAdapter adapter;

    @NonNull
    @Override
    public IRecordsListPresenter.VA createPresenter() {
        return new RecordsListPresenter(new RecordsListModel());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.records_list_screen);
        adapter = new NotesEventsAdapter(this);
        adapter.setNotesEventsAdapterClickListener(adapterClickListener);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        IRecordsListPresenter.VA presenter = getPresenter();
        if (presenter != null) {
            presenter.bindView(this);
        }
       // Broadcaster.registerReceiver(receiver, );
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_add:
                if (getPresenter() != null) {
                    getPresenter().onAddItemClicked();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void drawRecords(@NonNull List<NoteEvent> result) {
        adapter.setNoteEvents(result);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void openCreateEventDialog() {
        startActivity(new Intent(RecordsListActivity.this, CreateEventActivity.class));
    }

    @Override
    public void startDetailsNotePage(@NonNull NoteEvent noteEvent) {
        startActivity(EventDetailsActivity.buildLaunchIntent(this, noteEvent));
    }

    @Override
    public void startEditNotePage(@NonNull NoteEvent noteEvent) {
       startActivity(EditEventActivity.buildIntent(this, noteEvent.getId()));
    }

    @Override
    public void showDeleteNoteDialog(@NonNull final NoteEvent noteEvent) {
        new AppDialogBuilder().showDeleteEventDialog(this, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (getPresenter() != null) {
                    getPresenter().onDeleteNoteEventConfirmed(noteEvent);
                }
            }
        });
    }

    @Override
    public void onNoteDeleted(@NonNull NoteEvent noteEvent) {
        showToast(getString(R.string.deleted) + " " + noteEvent.getNoteText());
        adapter.notifyNoteDeleted(noteEvent.getId());
    }

    private NotesEventsAdapterClickListener adapterClickListener = new NotesEventsAdapterClickListener() {
        @Override
        public void onClick(int position, @NonNull NoteEvent event) {
            if (getPresenter() != null) {
                getPresenter().onClickNoteEvent(event);
            }
        }

        @Override
        public void onDelete(int position, @NonNull NoteEvent event) {
            if (getPresenter() != null) {
                getPresenter().onDeleteNoteEvent(event);
            }
        }

        @Override
        public void onEdit(int position, @NonNull NoteEvent event) {
            if (getPresenter() != null) {
                getPresenter().onEditNoteEvent(event);
            }
        }
    };
}
