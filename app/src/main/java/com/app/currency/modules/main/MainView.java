package com.app.currency.modules.main;

import com.app.currency.common.IBaseVA;

public interface MainView extends IBaseVA {

    void goToNotesEvents();

    void goToCurrencyCourse();

    void goToSettings();

    void goToProgramInfo();

}
