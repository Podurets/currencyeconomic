package com.app.currency.modules.hryvnyacourse;

import android.support.annotation.NonNull;

import com.app.currency.common.IBaseVA;
import com.app.currency.model.Currency;

import java.util.Date;
import java.util.List;

public interface CourseHryvnyaView extends IBaseVA {

    void drawCurrencies(@NonNull List<Currency> currencies);
    void showDateDialog(@NonNull Date date);
    void refreshDateInView(String dateText);

}
