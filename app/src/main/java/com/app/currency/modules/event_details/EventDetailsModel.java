package com.app.currency.modules.event_details;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.app.currency.App;
import com.app.currency.R;
import com.app.currency.common.BaseModel;
import com.app.currency.db.NoteEventDbHelper;
import com.app.currency.interfaces.ResultCallback;
import com.app.currency.model.NoteEvent;
import com.app.currency.util.SettingsConfigurator;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EventDetailsModel extends BaseModel<IEventDetailsViewModel.MA> implements IEventDetailsModel {

    @Nullable
    private NoteEvent noteEvent;
    private SimpleDateFormat dateFormat = SettingsConfigurator.getSettingsConfigurator().getDefaultSimpleDateFormat();

    @Override
    public void attachNoteEvent(@NonNull NoteEvent event) {
        this.noteEvent = event;
    }

    @Override
    public void requestNoteEventInfo() {
        if (noteEvent != null) {
            if (presenter() != null) {
                String eventDate = noteEvent.getNoteDate() > 0 ? dateFormat.format(new Date(noteEvent.getNoteDate())) : null;
                String notificationDate = noteEvent.getNotificationDate() > 0 ? dateFormat.format(new Date(noteEvent.getNotificationDate())) : null;
                presenter().drawNoteEventInfo(noteEvent.getNoteText(), noteEvent.getDescriptionText(),
                        eventDate, notificationDate);
            }
        }
    }

    @Override
    public int getEventId() {
        if (noteEvent != null) {
            return noteEvent.getId();
        }
        return 0;
    }

    @Override
    public void deleteNoteEvent() {
        if (noteEvent == null) {
            if (presenter() != null) {
                presenter().onError(App.getApp().getString(R.string.record_delete_error_message));
            }
            return;
        }
        new NoteEventDbHelper().deleteRecord(noteEvent.getId(), new ResultCallback<Boolean>() {
            @Override
            public void onResult(@Nullable Boolean aBoolean) {
                if (presenter() != null) {
                    presenter().onNoteDeleted();
                }
            }
        });
    }

}
