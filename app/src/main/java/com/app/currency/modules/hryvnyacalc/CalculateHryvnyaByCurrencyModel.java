package com.app.currency.modules.hryvnyacalc;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.app.currency.common.BaseModel;
import com.app.currency.model.Currency;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CalculateHryvnyaByCurrencyModel extends BaseModel<ICalculateHryvnyaByCurrencyPresenter.MA>
        implements ICalculateHryvnyaByCurrencyModel {

    private static final int DEFAULT_CURRENCY_SCALE_PRECISION = 6;

    @Nullable
    private Currency currency;

    @Override
    public void notifyCurrency(@NonNull Currency currency) {
        this.currency = currency;
    }

    @Nullable
    @Override
    public Currency getCurrency() {
        return currency;
    }

    @Override
    public double calculateCurrency(double value) {
        if (currency != null) {
            BigDecimal calc = new BigDecimal(currency.getRate());
            calc = calc.multiply(new BigDecimal(value));
            return calc.setScale(DEFAULT_CURRENCY_SCALE_PRECISION, RoundingMode.CEILING).doubleValue();
        }
        return 0;
    }
}
