package com.app.currency.modules.main;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.app.currency.R;
import com.app.currency.common.PresenterActivity;
import com.app.currency.databinding.ActivityMainBinding;
import com.app.currency.db.CurrencyDbHelper;
import com.app.currency.interfaces.NetworkCallback;
import com.app.currency.model.Currency;
import com.app.currency.modules.hryvnyacourse.CourseHryvnyaActivity;
import com.app.currency.modules.recordslist.RecordsListActivity;
import com.app.currency.modules.recordslist.RecordsListPresenter;
import com.app.currency.network.NetworkHelper;
import com.app.currency.util.DebugLogger;

import java.util.List;

public class MainActivity extends PresenterActivity<IMainPresenter.VA> implements MainView {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        viewDataBinding.setMainView(this);
    }

    @Override
    public boolean isDisplayBackButton() {
        return false;
    }

    @NonNull
    @Override
    public MainPresenter createPresenter() {
        return new MainPresenter(new MainModel());
    }

    @Override
    public void goToNotesEvents() {
        startActivity(new Intent(MainActivity.this, RecordsListActivity.class));
    }

    @Override
    public void goToCurrencyCourse() {

    }

    @Override
    public void goToSettings() {
        showToast("goToSettings");
    }

    @Override
    public void goToProgramInfo() {
        showToast("goToProgramInfo");
    }
}
