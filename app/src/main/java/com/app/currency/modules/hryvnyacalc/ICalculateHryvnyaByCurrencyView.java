package com.app.currency.modules.hryvnyacalc;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.app.currency.common.IBaseVA;

public interface ICalculateHryvnyaByCurrencyView extends IBaseVA {

    void updateCurrencyName(@NonNull String name);
    void updateCalculatedConvertedResult(@Nullable String result);

}
