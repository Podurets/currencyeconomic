package com.app.currency.modules.recordslist;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.app.currency.common.BaseModel;
import com.app.currency.db.NoteEventDbHelper;
import com.app.currency.interfaces.ResultCallback;
import com.app.currency.model.NoteEvent;

import java.util.List;

public class RecordsListModel extends BaseModel<IRecordsListPresenter.MA> implements IRecordsListModel {

    private NoteEventDbHelper helper = new NoteEventDbHelper();

    @Override
    public void loadNoteEvents() {
        helper.getAll(resultCallback);
    }

    @Override
    public void deleteNote(@NonNull final NoteEvent noteEvent) {
        new NoteEventDbHelper().deleteRecord(noteEvent.getId(), new ResultCallback<Boolean>() {
            @Override
            public void onResult(@Nullable Boolean aBoolean) {
                if (presenter() != null) {
                    if (aBoolean != null && aBoolean) {
                        presenter().onNoteDeleted(noteEvent);
                    } else {
                        presenter().onErrorDeleteNote(noteEvent);
                    }
                }
            }
        });
    }

    private ResultCallback<List<NoteEvent>> resultCallback = new ResultCallback<List<NoteEvent>>() {
        @Override
        public void onResult(@Nullable List<NoteEvent> noteEvents) {
            if (presenter() != null) {
                presenter().onRecordsLoaded(noteEvents);
            }
        }
    };
}
