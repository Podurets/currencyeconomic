package com.app.currency.modules.createevent;

import com.app.currency.modules.common.IManageEventModel;

public interface ICreateEventModel extends IManageEventModel<ICreateEventViewModel.MA> {

}
