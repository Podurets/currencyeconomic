package com.app.currency.modules;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.app.currency.util.ResourceManager;
import com.app.currency.util.Theme;
import com.app.currency.util.ThemeManager;

public abstract class ThemedActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateTheme();
    }

    public void updateTheme() {
        Theme theme = ThemeManager.getTheme(this);
        int themeId = ResourceManager.getThemeId(theme);
        if (themeId != 0) {
            setTheme(themeId);
        }
    }

    @Override
    public boolean isDisplayBackButton() {
        return true;
    }
}