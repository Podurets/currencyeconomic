package com.app.currency.modules.createevent;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.app.currency.R;
import com.app.currency.common.PresenterActivity;
import com.app.currency.interfaces.ResultCallback;
import com.app.currency.util.DateTimePicker;

import java.util.Date;

public class CreateEventActivity extends PresenterActivity<ICreateEventViewModel.VA> implements ICreateEventView {

    private Button event_date_btn;
    private Button notification_event_date_btn;
    private EditText title_et;
    private EditText description_et;

    private DateTimePicker dateTimePicker = new DateTimePicker();

    @NonNull
    @Override
    public ICreateEventViewModel.VA createPresenter() {
        return new CreateEventViewModel(new CreateEventModel());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindView(this);
        setContentView(R.layout.create_event_activity);
        event_date_btn = findViewById(R.id.event_date_btn);
        title_et = findViewById(R.id.title_et);
        description_et = findViewById(R.id.description_et);
        notification_event_date_btn = findViewById(R.id.notification_event_date_btn);
        event_date_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getPresenter() != null) {
                    getPresenter().onDateEventClicked();
                }
            }
        });
        notification_event_date_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getPresenter() != null) {
                    getPresenter().onNotificationDateClicked();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.accept, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accept:
                if (getPresenter() != null) {
                    getPresenter().onEventDataAccepted(title_et.getText().toString(), description_et.getText().toString());
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dateTimePicker.release();
    }

    @Override
    public void onSuccessSavedEvent() {
       showToast(getString(R.string.record_saved));
        finish();
    }

    @Override
    public void showDateDialog(final long dateTime) {
        dateTimePicker.setDateResultCallback(new ResultCallback<Date>() {
            @Override
            public void onResult(@Nullable Date date) {
                if (date != null) {
                    if (getPresenter() != null) {
                        getPresenter().onDateEventAccepted(date.getTime());
                    }
                }
            }
        });
        dateTimePicker.showDialog(this, dateTime);
    }

    @Override
    public void showNotificationDateDialog(long dateTime) {
        dateTimePicker.setDateResultCallback(new ResultCallback<Date>() {
            @Override
            public void onResult(@Nullable Date date) {
                if (date != null) {
                    if (getPresenter() != null) {
                        getPresenter().onNotificationDateAccepted(date.getTime());
                    }
                }
            }
        });
        dateTimePicker.showDialog(this, dateTime);
    }

    @Override
    public void setDateText(String text) {
        event_date_btn.setText(text);
    }

    @Override
    public void setNotificationDateText(String text) {
        notification_event_date_btn.setText(text);
    }
}
