package com.app.currency.modules.hryvnyacalc;

import android.support.annotation.NonNull;

import com.app.currency.App;
import com.app.currency.R;
import com.app.currency.common.BasePresenter;
import com.app.currency.model.Currency;

public class CalculateHryvnyaByCurrencyPresenter extends BasePresenter<ICalculateHryvnyaByCurrencyView,
        ICalculateHryvnyaByCurrencyModel> implements ICalculateHryvnyaByCurrencyPresenter.MA,
        ICalculateHryvnyaByCurrencyPresenter.VA {

    public CalculateHryvnyaByCurrencyPresenter(@NonNull ICalculateHryvnyaByCurrencyModel iCalculateHryvnyaByCurrencyModel) {
        super(iCalculateHryvnyaByCurrencyModel);
    }

    @Override
    protected void updateView() {

    }

    @Override
    public void notifyCurrency(@NonNull Currency currency) {
        ICalculateHryvnyaByCurrencyModel model = model();
        if (model != null) {
            model.notifyCurrency(currency);
        }
    }

    @Override
    public void notifyUpdateViews() {
        ICalculateHryvnyaByCurrencyModel model = model();
        if (model != null) {
            Currency currency = model.getCurrency();
            ICalculateHryvnyaByCurrencyView view = view();
            if (currency != null && view != null) {
                view.updateCurrencyName(currency.getName());
            }
        }
    }

    @Override
    public void onSomeValueInputed(double value) {
        ICalculateHryvnyaByCurrencyModel model = model();
        if (model == null) {
            return;
        }
        double calculatedValue = model.calculateCurrency(value);
        ICalculateHryvnyaByCurrencyView view = view();
        if (view != null) {
            view.updateCalculatedConvertedResult(App.getApp().getString(R.string.result) + " " + calculatedValue);
        }
    }
}
