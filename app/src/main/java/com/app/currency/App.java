package com.app.currency;

import android.app.Application;
import android.text.TextUtils;
import android.widget.Toast;

import com.app.currency.network.ICurrencyApi;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class App extends Application {

    private static App app;
    private Retrofit retrofit;
    private ICurrencyApi currencyApi;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        retrofit = new Retrofit.Builder()
                .baseUrl("https://bank.gov.ua/NBUStatService/v1/") //Базовая часть адреса
                .addConverterFactory(GsonConverterFactory.create()) //Конвертер, необходимый для преобразования JSON'а в объекты
                .build();
        currencyApi = retrofit.create(ICurrencyApi.class);
    }

    public ICurrencyApi getCurrencyApi(){
        return currencyApi;
    }

    public static App getApp() {
        return app;
    }

    public static void showToast(CharSequence text){
        if(!TextUtils.isEmpty(text)){
            Toast.makeText(app, text, Toast.LENGTH_SHORT).show();
        }
    }
}
