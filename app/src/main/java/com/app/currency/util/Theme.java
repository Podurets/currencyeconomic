package com.app.currency.util;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.app.currency.R;

public enum Theme {

    Indigo(R.string.indigo_theme),
    Purple(R.string.purple_theme), Grey(R.string.grey_theme),
    Amber(R.string.amber_theme);

    private final String ColorPrimarySuffix = "ColorPrimary";
    private final String ColorPrimaryDarkSuffix = "ColorPrimaryDark";
    private final String ColorAccentSuffix = "ColorAccent";
    private final String RippleColorSuffix = "RippleColor";
    private final String LineDividerSuffix = "LineDividerColor";
    private final String TextColorSuffix = "TextColor";
    private final String TextColorPrimarySuffix = "TextColorPrimary";

    public final int themeLabelId;

    Theme(@StringRes int themeLabel) {
        this.themeLabelId = themeLabel;
    }

    @NonNull
    public static Theme getDefault() {
        return Indigo;
    }

    public String getLabel(Context context) {
        return context.getString(themeLabelId);
    }

    public int getPrimaryColor(Context context){
        return ResourceManager.getColor(context, name()+ColorPrimarySuffix);
    }
    public int getColorPrimaryDark(Context context){
        return ResourceManager.getColor(context, name()+ColorPrimaryDarkSuffix);

    }public int getTextColorPrimary(Context context){
        return ResourceManager.getColor(context, name()+TextColorPrimarySuffix);
    }
    public int getColorAccent(Context context){
        return ResourceManager.getColor(context, name()+ColorAccentSuffix);
    }
    public int getRippleColor(Context context){
        return ResourceManager.getColor(context, name()+RippleColorSuffix);
    }
    public int getLineDividerColor(Context context){
        return ResourceManager.getColor(context, name()+LineDividerSuffix);
    }
    public int getTextColor(Context context){
        return ResourceManager.getColor(context, name()+TextColorSuffix);
    }

    public int getHighLightColor(Context context){
        return getRippleColor(context);
    }

    @NonNull
    public static ColorFilter getMainDarkIconFilter(){
        return new PorterDuffColorFilter(Color.parseColor("#311B92"), PorterDuff.Mode.SRC_IN);
    }  @NonNull
    public static ColorFilter getMainLightIconFilter(){
        return new PorterDuffColorFilter(Color.parseColor("#6200EA"), PorterDuff.Mode.SRC_IN);
    }
}
