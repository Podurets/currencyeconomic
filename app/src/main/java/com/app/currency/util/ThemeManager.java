package com.app.currency.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import com.app.currency.App;

public class ThemeManager {
    public static final String THEME_SHARED_PREF_KEY = "theme_key";

    public static void setTheme(Context context, @NonNull Theme theme) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putString(THEME_SHARED_PREF_KEY, theme.name()).apply();
    }

    public static Theme getTheme(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String name = prefs.getString(THEME_SHARED_PREF_KEY, Theme.getDefault().name());
        try {
            return Theme.valueOf(name);
        } catch (Exception e) {
            setTheme(App.getApp(), Theme.getDefault());
            return Theme.getDefault();
        }
    }
}
