package com.app.currency.util;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;

import com.app.currency.App;

public class Broadcaster {

    public static final String EVENT_NOTE_CREATED_ACTION = "ev_note_created";
    public static final String EVENT_NOTE_UPDATED_ACTION = "ev_note_updated";
    public static final String EVENT_NOTE_DELETED_ACTION = "ev_note_deleted";


    public static void sendBroadcast(@NonNull String action){
        LocalBroadcastManager.getInstance(App.getApp()).sendBroadcast(new Intent(action));
    }

    public static void sendRecordUpdatedBroadcast(@NonNull String action, int recordId){
        Intent intent = new Intent(action);
        intent.putExtra(AppConstants.INTENT_DATA_KEY, recordId);
        LocalBroadcastManager.getInstance(App.getApp()).sendBroadcast(intent);
    }

    public static void registerReceiver(@NonNull BroadcastReceiver receiver, @NonNull IntentFilter intentFilter) {
        try {
            LocalBroadcastManager.getInstance(App.getApp()).registerReceiver(receiver, intentFilter);
        } catch (Exception ignore) {

        }
    }

    public static void unregisterReceiver(@NonNull BroadcastReceiver receiver) {
        try {
            LocalBroadcastManager.getInstance(App.getApp()).unregisterReceiver(receiver);
        } catch (Exception ignore) {

        }
    }

}
