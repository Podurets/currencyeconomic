package com.app.currency.util;

import android.text.TextUtils;
import android.util.Log;

import com.app.currency.BuildConfig;

public class DebugLogger {

    public static void e(String tag, String message) {
        if (BuildConfig.DEBUG && !TextUtils.isEmpty(message)) {
            Log.e(TextUtils.isEmpty(tag) ? DebugLogger.class.getSimpleName() : tag, message);
        }
    }

    public static void e(String tag, String message, Throwable e) {
        if (BuildConfig.DEBUG && !TextUtils.isEmpty(message)) {
            Log.e(TextUtils.isEmpty(tag) ? DebugLogger.class.getSimpleName() : tag, message, e);
        }
    }

    public static void w(String tag, String message) {
        if (BuildConfig.DEBUG && !TextUtils.isEmpty(message)) {
            Log.w(TextUtils.isEmpty(tag) ? DebugLogger.class.getSimpleName() : tag, message);
        }
    }

    public static void v(String tag, String message) {
        if (BuildConfig.DEBUG && !TextUtils.isEmpty(message)) {
            Log.v(TextUtils.isEmpty(tag) ? DebugLogger.class.getSimpleName() : tag, message);
        }
    }

}
