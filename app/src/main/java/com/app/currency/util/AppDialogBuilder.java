package com.app.currency.util;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.app.currency.R;
import com.app.currency.interfaces.ResultCallback;
import com.app.currency.model.NoteEvent;

public class AppDialogBuilder extends DialogBuilder {
    @Override
    public int getDialogTheme() {
        return 0;
    }

    @Nullable
    public AlertDialog showCreateEventDialog(@NonNull Activity activity){
         if(isAliveActivity(activity)){
           AlertDialog.Builder builder = getDialogBuilder(activity);
           View view = LayoutInflater.from(activity).inflate(R.layout.create_event_activity, null);
           builder.setView(R.layout.create_event_activity);
             AlertDialog dialog = builder.create();
             dialog.show();
            Window window = dialog.getWindow();
            if(window!=null){
              View rootView =  window.getDecorView();
            }

             return dialog;
         }
         return null;
    }

    @Nullable
    public AlertDialog showDeleteEventDialog(@NonNull Activity activity, DialogInterface.OnClickListener acceptListener){
        if(isAliveActivity(activity)){
           return showAskDialog(activity, activity.getString(R.string.attention), activity.getString(R.string.delete_record_question),
                    activity.getString(R.string.yes), acceptListener,
                    activity.getString(R.string.cancel), null);
        }
        return null;
    }

}
