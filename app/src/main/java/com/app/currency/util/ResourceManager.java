package com.app.currency.util;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;

import com.app.currency.App;

public class ResourceManager {

    public static final int getThemeId(@NonNull Theme theme) {
        Context context = App.getApp();
        return context.getResources().getIdentifier(theme.name(), "style", context.getPackageName());
    }

    public static int getColor(@NonNull Context context, @NonNull String colorName){
        int colorId = context.getResources().getIdentifier(colorName, "color", context.getPackageName());
        return colorId == 0 ? Color.BLACK : ContextCompat.getColor(context, colorId);
    }
}
