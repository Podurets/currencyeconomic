package com.app.currency.util;

import android.support.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class SettingsConfigurator {

    private static volatile SettingsConfigurator instanceSettingsConfigurator;

    public static SettingsConfigurator getSettingsConfigurator() {
        SettingsConfigurator localSettingsConfigurator = instanceSettingsConfigurator;
        if (localSettingsConfigurator == null) {
            synchronized (SettingsConfigurator.class) {
                localSettingsConfigurator = instanceSettingsConfigurator;
                if (localSettingsConfigurator == null) {
                    localSettingsConfigurator = instanceSettingsConfigurator = new SettingsConfigurator();
                }
            }
        }
        return localSettingsConfigurator;
    }

    private SettingsConfigurator() {
    }

    @NonNull
    public final Locale getDatePrintLocale(){
       return new Locale("uk", "UA");
    }

    public final SimpleDateFormat getDefaultSimpleDateFormat() {
        return new SimpleDateFormat("HH:mm, dd MMMM yyyy", getDatePrintLocale());
    }
}
