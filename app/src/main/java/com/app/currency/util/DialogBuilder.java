package com.app.currency.util;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;

public abstract class DialogBuilder {

    public interface InputDialogListener {
        void onAccept(CharSequence inputText);

        void onCancel();
    }

    @NonNull
    protected final AlertDialog.Builder getDialogBuilder(@NonNull Activity activity) {
        if (getDialogTheme() > 0) {
            return new AlertDialog.Builder(activity, getDialogTheme());
        } else {
            return new AlertDialog.Builder(activity);
        }
    }

    @Nullable
    public AlertDialog showInputDialog(@NonNull Activity activity, CharSequence title, CharSequence message,
                                       CharSequence positiveButtonText, CharSequence negativeButtonText,
                                       final InputDialogListener acceptListener,
                                       CharSequence text) {
        if (!isAliveActivity(activity)) {
            return null;
        }
        AlertDialog.Builder alertBuilder = getDialogBuilder(activity);
        final AppCompatEditText editText = new AppCompatEditText(alertBuilder.getContext());
        editText.setText(text);
        alertBuilder.setMessage(message);
        alertBuilder.setTitle(title);
        alertBuilder.setView(editText);
        alertBuilder.setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if (acceptListener != null) {
                    acceptListener.onAccept(editText.getText());
                }
            }
        });
        alertBuilder.setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if (acceptListener != null) {
                    acceptListener.onCancel();
                }
            }
        });
        editText.post(new Runnable() {
            @Override
            public void run() {
                editText.setSelection(editText.getText().length());
            }
        });
        return alertBuilder.show();
    }

    @Nullable
    public AlertDialog showInfoDialog(@NonNull Activity activity, CharSequence title, CharSequence message,
                                      CharSequence positiveButtonText, final DialogInterface.OnClickListener acceptListener) {
        if (!isAliveActivity(activity)) {
            return null;
        }
        AlertDialog.Builder alertBuilder = getDialogBuilder(activity);
        alertBuilder.setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if (acceptListener != null) {
                    acceptListener.onClick(dialog, whichButton);
                }
            }
        });
        alertBuilder.setMessage(message);
        alertBuilder.setTitle(title);
        return alertBuilder.show();
    }

    @Nullable
    public AlertDialog showAskDialog(@NonNull Activity activity, CharSequence title, CharSequence message,
                                     CharSequence positiveButtonText, DialogInterface.OnClickListener acceptListener,
                                     final CharSequence negativeButtonText, DialogInterface.OnClickListener declineListener) {
        if (!isAliveActivity(activity)) {
            return null;
        }
        AlertDialog.Builder alertBuilder = getDialogBuilder(activity);
        alertBuilder.setPositiveButton(positiveButtonText, acceptListener);
        alertBuilder.setNegativeButton(negativeButtonText, declineListener);
        alertBuilder.setMessage(message);
        alertBuilder.setTitle(title);
        return alertBuilder.show();
    }

    public abstract int getDialogTheme();

    public static boolean isAliveActivity(@Nullable Activity activity) {
        return activity != null && !activity.isFinishing() && !activity.isDestroyed();
    }
}