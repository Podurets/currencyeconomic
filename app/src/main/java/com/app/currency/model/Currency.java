package com.app.currency.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Currency extends DbModel implements Parcelable{

    @SerializedName("r030")
    private int r030;
    @SerializedName("txt")
    private String name;
    @SerializedName("rate")
    private double rate;
    @SerializedName("cc")
    private String currencyCode;
    @SerializedName("exchangedate")
    private String exchangeDate;

    public int getR030() {
        return r030;
    }

    public void setR030(int r030) {
        this.r030 = r030;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getExchangeDate() {
        return exchangeDate;
    }

    public void setExchangeDate(String exchangeDate) {
        this.exchangeDate = exchangeDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.getId());
        dest.writeInt(this.r030);
        dest.writeString(this.name);
        dest.writeDouble(this.rate);
        dest.writeString(this.currencyCode);
        dest.writeString(this.exchangeDate);
    }

    public Currency() {
    }

    protected Currency(Parcel in) {
        this.setId(in.readInt());
        this.r030 = in.readInt();
        this.name = in.readString();
        this.rate = in.readDouble();
        this.currencyCode = in.readString();
        this.exchangeDate = in.readString();
    }

    public static final Creator<Currency> CREATOR = new Creator<Currency>() {
        @Override
        public Currency createFromParcel(Parcel source) {
            return new Currency(source);
        }

        @Override
        public Currency[] newArray(int size) {
            return new Currency[size];
        }
    };
}
