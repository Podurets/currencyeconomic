package com.app.currency.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class NoteEvent extends DbModel implements Parcelable{

    @NonNull
    private String noteText;
    @Nullable
    private String descriptionText;
    private long noteDate;
    private long notificationDate;

    public NoteEvent(@NonNull String noteText) {
        this.noteText = noteText;
    }

    @NonNull
    public String getNoteText() {
        return noteText;
    }

    public void setNoteText(@NonNull String noteText) {
        this.noteText = noteText;
    }

    public long getNoteDate() {
        return noteDate;
    }

    public void setNoteDate(long noteDate) {
        this.noteDate = noteDate;
    }

    @Nullable
    public String getDescriptionText() {
        return descriptionText;
    }

    public void setDescriptionText(@Nullable String descriptionText) {
        this.descriptionText = descriptionText;
    }

    public long getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(long notificationDate) {
        this.notificationDate = notificationDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.noteText);
        dest.writeString(this.descriptionText);
        dest.writeLong(this.noteDate);
        dest.writeLong(this.notificationDate);
        dest.writeInt(this.getId());
    }

    protected NoteEvent(Parcel in) {
        this.noteText = in.readString();
        this.descriptionText = in.readString();
        this.noteDate = in.readLong();
        this.notificationDate = in.readLong();
        this.setId(in.readInt());
    }

    public static final Creator<NoteEvent> CREATOR = new Creator<NoteEvent>() {
        @Override
        public NoteEvent createFromParcel(Parcel source) {
            return new NoteEvent(source);
        }

        @Override
        public NoteEvent[] newArray(int size) {
            return new NoteEvent[size];
        }
    };
}
