package com.app.currency.common;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.lang.ref.WeakReference;

public abstract class BasePresenter<View extends IBaseVA, Model extends IBaseMA> implements IBasePA.VA, IBasePA.MA{

    @Nullable
    private WeakReference<View> view;
    @Nullable
    private Model model;

    public BasePresenter(@NonNull Model model) {
        setModel(model);
    }

    public void setModel(@NonNull Model model) {
        resetState();
        this.model = model;
        model.attachViewModel(this);
        if (setupDone()) {
            updateView();
        }
    }

    protected void resetState() {
    }

    @Override
    public void bindView(IBaseVA va) {
        this.view = new WeakReference<>((View) va);
        if (setupDone()) {
            updateView();
        }
    }

    public void unbindView() {
        this.view = null;
    }

    @Nullable
    protected View view() {
        if (view == null) {
            return null;
        } else {
            return view.get();
        }
    }

    @Override
    public void onDestroy() {
        destroy();
    }

    @Override
    public void onError(@NonNull String message) {
        if (view() != null) {
            view().showToast(message);
        }
    }

    @Nullable
    protected Model model(){
        return model;
    }

     private void destroy() {
        model = null;
        view = null;
    }
    protected abstract void updateView();

    protected boolean setupDone() {
        return view() != null && model != null;
    }

}
