package com.app.currency.common;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.app.currency.modules.ThemedActivity;

public abstract class PresenterActivity<Presenter extends IBasePA.VA> extends ThemedActivity implements IBaseVA {

    @Nullable
    private Presenter presenter;

    @Nullable
    public Presenter getPresenter() {
        return presenter;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = createPresenter();
    }

    protected void bindView(@NonNull IBaseVA view) {
        if (presenter != null) {
            presenter.bindView(view);
        }
    }

    @NonNull
    public abstract Presenter createPresenter();

    @Override
    protected void onDestroy() {
        if (presenter != null) {
            presenter.onDestroy();
        }
        super.onDestroy();
    }

    @Override
    public void showToast(@NonNull String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }
}
