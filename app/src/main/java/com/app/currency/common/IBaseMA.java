package com.app.currency.common;


import android.support.annotation.NonNull;

public interface IBaseMA<ViewModel extends IBasePA.MA> {

    void onDestroy();
    void attachViewModel(@NonNull ViewModel view);

}
