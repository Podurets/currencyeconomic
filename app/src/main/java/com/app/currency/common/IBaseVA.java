package com.app.currency.common;

import android.support.annotation.NonNull;

public interface IBaseVA {

    void showToast(@NonNull String message);

}
