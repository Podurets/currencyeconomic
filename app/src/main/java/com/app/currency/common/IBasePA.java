package com.app.currency.common;

import android.support.annotation.NonNull;

public interface IBasePA {

    interface VA {
        void bindView(IBaseVA va);
        void onDestroy();
    }

    interface MA{
        void onError(@NonNull String message);
    }

}
