package com.app.currency.common;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class BaseModel<ViewModel extends IBasePA.MA> implements IBaseMA<ViewModel> {

    @Nullable
    private ViewModel viewModel;

    public BaseModel() {
    }

    public BaseModel(@NonNull ViewModel viewModel) {
        this.viewModel = viewModel;
    }

    @Override
    public void attachViewModel(@NonNull ViewModel viewModel) {
        this.viewModel = viewModel;
    }

    void detachViewModel() {
        this.viewModel = null;
    }

    @Nullable
    protected ViewModel presenter() {
        return viewModel;
    }

    @Override
    public void onDestroy() {
        detachViewModel();
    }

}
