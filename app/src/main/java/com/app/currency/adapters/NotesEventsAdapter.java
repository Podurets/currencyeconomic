package com.app.currency.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.currency.R;
import com.app.currency.interfaces.NotesEventsAdapterClickListener;
import com.app.currency.interfaces.PositionClickListener;
import com.app.currency.model.NoteEvent;
import com.app.currency.util.Theme;

import java.util.ArrayList;
import java.util.List;

public class NotesEventsAdapter extends RecyclerView.Adapter<NotesEventsAdapter.Holder> {

    @NonNull
    private LayoutInflater inflater;
    @NonNull
    private List<NoteEvent> noteEvents = new ArrayList<>();
    private NotesEventsAdapterClickListener notesEventsAdapterClickListener;

    public NotesEventsAdapter(@NonNull Context context) {
        this.inflater = LayoutInflater.from(context);
    }

    public void setNotesEventsAdapterClickListener(NotesEventsAdapterClickListener notesEventsAdapterClickListener) {
        this.notesEventsAdapterClickListener = notesEventsAdapterClickListener;
    }

    public void setNoteEvents(@NonNull List<NoteEvent> noteEvents) {
        this.noteEvents = noteEvents;
    }

    private PositionClickListener positionClickListener = new PositionClickListener() {
        @Override
        public void onClick(int position) {
            if (notesEventsAdapterClickListener != null) {
                if (noteEvents.size() > position) {
                    notesEventsAdapterClickListener.onClick(position, noteEvents.get(position));
                }
            }
        }
    };

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return Holder.init(inflater, parent);
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {
        final NoteEvent noteEvent = noteEvents.get(position);
        holder.title.setText(noteEvent.getNoteText());
        holder.description.setText(noteEvent.getDescriptionText());
        boolean isDateNote = noteEvent.getNoteDate() > 0 || noteEvent.getNotificationDate() > 0;
        holder.imageIcon.setImageResource(isDateNote ? R.drawable.ic_notification_event_note : R.drawable.ic_event_note);
        holder.setItemClickListener(positionClickListener);
        holder.deleteView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (notesEventsAdapterClickListener != null) {
                    int position = holder.getAdapterPosition();
                    notesEventsAdapterClickListener.onDelete(position, noteEvent);
                }
            }
        });
        holder.editView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (notesEventsAdapterClickListener != null) {
                    int position = holder.getAdapterPosition();
                    notesEventsAdapterClickListener.onEdit(position, noteEvent);
                }
            }
        });
        holder.imageIcon.setColorFilter(Theme.getMainLightIconFilter());
        holder.deleteView.setColorFilter(Theme.getMainDarkIconFilter());
        holder.editView.setColorFilter(Theme.getMainDarkIconFilter());
    }

    @Override
    public int getItemCount() {
        return noteEvents.size();
    }

    public void notifyNoteDeleted(int noteId) {
        for (int i = 0; i < noteEvents.size(); i++) {
            if (noteEvents.get(i).getId() == noteId) {
                noteEvents.remove(i);
                notifyItemRemoved(i);
                return;
            }
        }
        notifyDataSetChanged();
    }

    public static class Holder extends ClickHolder {

        public final ImageView imageIcon;
        public final TextView title;
        public final TextView description;
        public final ImageView editView;
        public final ImageView deleteView;

        public Holder(View itemView) {
            super(itemView);
            imageIcon = itemView.findViewById(R.id.imageIcon);
            title = itemView.findViewById(R.id.title_tv);
            description = itemView.findViewById(R.id.description_tv);
            editView = itemView.findViewById(R.id.iv_edit);
            deleteView = itemView.findViewById(R.id.iv_delete);
        }

        public static Holder init(@NonNull LayoutInflater inflater, @Nullable ViewGroup viewGroup) {
            return new Holder(inflater.inflate(R.layout.notes_event_item, viewGroup, false));
        }

    }

}
