package com.app.currency.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.currency.R;
import com.app.currency.interfaces.ListItemClickListener;
import com.app.currency.interfaces.PositionClickListener;
import com.app.currency.model.Currency;

import java.util.ArrayList;
import java.util.List;

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.CurrencyHolder> {

    private LayoutInflater inflater;
    @NonNull
    private List<Currency> currencies = new ArrayList<>();
    private ListItemClickListener<Currency> listItemClickListener;

    public CurrencyAdapter(@NonNull Context context) {
        this.inflater = LayoutInflater.from(context);
    }

    public ListItemClickListener<Currency> getListItemClickListener() {
        return listItemClickListener;
    }

    public void setListItemClickListener(ListItemClickListener<Currency> listItemClickListener) {
        this.listItemClickListener = listItemClickListener;
    }

    public void setCurrencies(@NonNull List<Currency> currencies) {
        this.currencies = currencies;
    }

    @Override
    public CurrencyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return CurrencyHolder.create(inflater, parent);
    }

    @Override
    public void onBindViewHolder(CurrencyHolder holder, int position) {
        Currency currency = currencies.get(position);
        holder.title.setText(currency.getName());
        holder.description.setText("Value: " + currency.getRate());
        holder.setItemClickListener(clickListener);
    }

    private PositionClickListener clickListener = new PositionClickListener() {
        @Override
        public void onClick(int position) {
            if (listItemClickListener != null) {
                if (currencies.size() > position) {
                    listItemClickListener.onClick(currencies.get(position));
                }
            }
        }
    };

    @Override
    public int getItemCount() {
        return currencies.size();
    }


    public static class CurrencyHolder extends ClickHolder {

        public final TextView title;
        public final TextView description;

        private CurrencyHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title_tv);
            description = itemView.findViewById(R.id.description_tv);
        }

        public static CurrencyHolder create(@NonNull LayoutInflater inflater, @Nullable ViewGroup parent) {
            return new CurrencyHolder(inflater.inflate(R.layout.currency_item, parent, false));
        }
    }

}
