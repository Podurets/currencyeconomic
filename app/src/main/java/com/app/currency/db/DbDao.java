package com.app.currency.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.app.currency.App;
import com.app.currency.interfaces.ResultCallback;
import com.app.currency.model.DbModel;
import com.app.currency.util.DebugLogger;

import java.util.ArrayList;
import java.util.List;

public abstract class DbDao<Model extends DbModel> {

    protected abstract ContentValues toValues(@NonNull Model model);

    @Nullable
    protected abstract Model fromCursor(@Nullable Cursor cursor);

    @NonNull
    protected abstract List<Model> fromListCursor(@Nullable Cursor cursor);

    @NonNull
    protected abstract String getTableName();

    @NonNull
    protected abstract String getIdColumnName();

    public void createOrUpdate(@NonNull Model model) {
        ContentValues values = toValues(model);
        if (values != null) {
            SQLiteDatabase db = AppSqliteDbHelper.getSqliteHelper(App.getApp()).getWritableDatabase();
            if (model.getId() > 0) {
                int count = db.update(getTableName(), values, getIdColumnName() + " = ?",
                        new String[]{String.valueOf(model.getId())});
                if (count <= 0) {
                    values.remove(getIdColumnName());
                    db.insert(getTableName(), null, values);
                }
            } else {
                db.insert(getTableName(), null, values);
            }
        }
    }

    public void deleteRecord(final int recordId, final ResultCallback<Boolean> resultCallback){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Boolean isSuccess = Boolean.FALSE;
                try {
                    SQLiteDatabase db = AppSqliteDbHelper.getSqliteHelper(App.getApp()).getWritableDatabase();
                    isSuccess = db.delete(getTableName(), getIdColumnName() + " = ?", new String[]{String.valueOf(recordId)}) > 0;
                } catch (Throwable e) {
                    DebugLogger.e(DbDao.class.getSimpleName(), "deleteRecord", e);
                }
                if (resultCallback != null) {
                    final Boolean finalIsSuccess = isSuccess;
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            resultCallback.onResult(finalIsSuccess);
                        }
                    });
                }
            }
        }).start();
    }


    @Nullable
    public Model get(int recordId) {
        SQLiteDatabase db = AppSqliteDbHelper.getSqliteHelper(App.getApp()).getWritableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.query(getTableName(), null, getIdColumnName() + " = ?",
                    new String[]{String.valueOf(recordId)}, null, null, null);
            return fromCursor(cursor);
        } catch (Exception e) {
            DebugLogger.e(DbDao.class.getSimpleName(), "get", e);
        } finally {
            AppSqliteDbHelper.closeCursor(cursor);
        }
        return null;
    }

    public void get(final int recordId, final ResultCallback<Model> resultCallback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final Model model = get(recordId);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (resultCallback != null) {
                            resultCallback.onResult(model);
                        }
                    }
                });
            }
        }).start();
    }

    public void getAll(final ResultCallback<List<Model>> resultCallback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                SQLiteDatabase db = AppSqliteDbHelper.getSqliteHelper(App.getApp()).getReadableDatabase();
                Cursor cursor = null;
                try {
                    cursor = db.query(getTableName(), null, null, null, null, null, null);
                    final List<Model> data = fromListCursor(cursor);
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if (resultCallback != null) {
                                resultCallback.onResult(data);
                            }
                        }
                    });
                    return;
                } catch (Exception e) {
                    DebugLogger.e(DbDao.class.getSimpleName(), "getAll", e);
                } finally {
                    AppSqliteDbHelper.closeCursor(cursor);
                }
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (resultCallback != null) {
                            resultCallback.onResult(new ArrayList<Model>());
                        }
                    }
                });
            }
        }).start();
    }

}
