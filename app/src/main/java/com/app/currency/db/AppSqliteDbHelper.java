package com.app.currency.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class AppSqliteDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "appDb";

    private static volatile AppSqliteDbHelper instanceSqliteHelper;

    public static AppSqliteDbHelper getSqliteHelper(@NonNull Context context) {
        AppSqliteDbHelper localSqliteHelper = instanceSqliteHelper;
        if (localSqliteHelper == null) {
            synchronized (AppSqliteDbHelper.class) {
                localSqliteHelper = instanceSqliteHelper;
                if (localSqliteHelper == null) {
                    localSqliteHelper = instanceSqliteHelper = new AppSqliteDbHelper(context);
                }
            }
        }
        return localSqliteHelper;
    }

    public AppSqliteDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CURRENCY_TABLE = "CREATE TABLE " + TableNames.Currency.TABLE_NAME + "("
                + TableNames.Currency.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + TableNames.Currency.NAME + " TEXT,"
                + TableNames.Currency.R030 + " INTEGER,"
                + TableNames.Currency.RATE + " REAL,"
                + TableNames.Currency.CURRENCY_CODE + " TEXT,"
                + TableNames.Currency.EXCHANGE_DATE + " TEXT" + ")";
        db.execSQL(CREATE_CURRENCY_TABLE);

        String CREATE_EVENT_NOTE_TABLE = "CREATE TABLE "+TableNames.EventNote.TABLE_NAME + "("
                + TableNames.EventNote.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + TableNames.EventNote.TEXT + " TEXT,"
                + TableNames.EventNote.DESCRIPTION + " timestamp not null default 0,"
                + TableNames.EventNote.NOTIFICATION_DATE + " timestamp not null default 0,"
                + TableNames.EventNote.DATE + " TEXT" + ")";
        db.execSQL(CREATE_EVENT_NOTE_TABLE);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TableNames.Currency.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TableNames.EventNote.TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    public static void closeCursor(@Nullable Cursor cursor) {
        try {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        } catch (Exception ignore) {

        }
    }

}
