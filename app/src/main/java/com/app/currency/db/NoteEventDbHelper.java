package com.app.currency.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.app.currency.model.NoteEvent;

import java.util.ArrayList;
import java.util.List;

public class NoteEventDbHelper extends DbDao<NoteEvent> {


    @Override
    protected ContentValues toValues(@NonNull NoteEvent noteEvent) {
        ContentValues values = new ContentValues();
        if (noteEvent.getId() > 0) {
            values.put(TableNames.EventNote.ID, noteEvent.getId());
        }
        values.put(TableNames.EventNote.TEXT, noteEvent.getNoteText());
        values.put(TableNames.EventNote.DESCRIPTION, noteEvent.getDescriptionText());
        values.put(TableNames.EventNote.DATE, noteEvent.getNoteDate());
        values.put(TableNames.EventNote.NOTIFICATION_DATE, noteEvent.getNotificationDate());
        return values;
    }

    @Override
    protected NoteEvent fromCursor(@Nullable Cursor cursor) {
        if (cursor != null && cursor.moveToFirst()) {
            String noteText = cursor.getString(cursor.getColumnIndex(TableNames.EventNote.TEXT));
            NoteEvent noteEvent = new NoteEvent(noteText);
            noteEvent.setId(cursor.getInt(cursor.getColumnIndex(TableNames.EventNote.ID)));
            noteEvent.setDescriptionText(cursor.getString(cursor.getColumnIndex(TableNames.EventNote.DESCRIPTION)));
            noteEvent.setNoteDate(cursor.getLong(cursor.getColumnIndex(TableNames.EventNote.DATE)));
            noteEvent.setNotificationDate(cursor.getLong(cursor.getColumnIndex(TableNames.EventNote.NOTIFICATION_DATE)));
            return noteEvent;
        }
        return null;
    }

    @NonNull
    @Override
    protected List<NoteEvent> fromListCursor(@Nullable Cursor cursor) {
        List<NoteEvent> events = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            int idIndex =   cursor.getColumnIndex(TableNames.EventNote.ID);
            int textIndex =   cursor.getColumnIndex(TableNames.EventNote.TEXT);
            int descriptionIndex =   cursor.getColumnIndex(TableNames.EventNote.DESCRIPTION);
            int dateIndex =   cursor.getColumnIndex(TableNames.EventNote.DATE);
            int notificationDateIndex =   cursor.getColumnIndex(TableNames.EventNote.NOTIFICATION_DATE);
            do {
                String noteText = cursor.getString(textIndex);
                NoteEvent noteEvent = new NoteEvent(noteText);
                noteEvent.setId(cursor.getInt(idIndex));
                noteEvent.setDescriptionText(cursor.getString(descriptionIndex));
                noteEvent.setNoteDate(cursor.getLong(dateIndex));
                noteEvent.setNotificationDate(cursor.getLong(notificationDateIndex));

                events.add(noteEvent);
            } while (cursor.moveToNext());
        }
        return events;
    }

    @NonNull
    @Override
    protected String getTableName() {
        return TableNames.EventNote.TABLE_NAME;
    }

    @NonNull
    @Override
    protected String getIdColumnName() {
        return TableNames.EventNote.ID;
    }
}
