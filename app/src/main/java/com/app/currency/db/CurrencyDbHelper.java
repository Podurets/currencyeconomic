package com.app.currency.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.app.currency.App;
import com.app.currency.model.Currency;
import com.app.currency.util.DebugLogger;

import java.util.ArrayList;
import java.util.List;

public class CurrencyDbHelper extends DbDao<Currency> {

    public void saveCurrencies(@NonNull List<Currency> currencies) {
        if (currencies.size() > 0) {
            SQLiteDatabase db = AppSqliteDbHelper.getSqliteHelper(App.getApp()).getWritableDatabase();
            for (int i = 0; i < currencies.size(); i++) {
                saveCurrency(db, currencies.get(i));
            }
        }
    }

    public void saveCurrenciesAsync(@NonNull final List<Currency> currencies) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                saveCurrencies(currencies);
            }
        }).start();
    }

    public void saveCurrency(@NonNull SQLiteDatabase db, @NonNull Currency currency) {
        ContentValues values = toValues(currency);
        if (currency.getId() > 0) {
            int count = db.update(TableNames.Currency.TABLE_NAME, values, TableNames.Currency.ID + " = ?",
                    new String[]{String.valueOf(currency.getId())});
            if (count <= 0) {
                values.remove(TableNames.Currency.ID);
                db.update(TableNames.Currency.TABLE_NAME, values, TableNames.Currency.EXCHANGE_DATE + " = ? AND " +
                        TableNames.Currency.CURRENCY_CODE + " = ?", new String[]{currency.getExchangeDate(), currency.getCurrencyCode()});
            }
        } else {

            int count = db.update(TableNames.Currency.TABLE_NAME, values, TableNames.Currency.EXCHANGE_DATE + " = ? AND " +
                    TableNames.Currency.CURRENCY_CODE + " = ?", new String[]{currency.getExchangeDate(), currency.getCurrencyCode()});

            if (count <= 0) {
                db.insert(TableNames.Currency.TABLE_NAME, null, values);
            }
        }
    }

    public void saveCurrency(@NonNull Currency currency) {
        SQLiteDatabase db = AppSqliteDbHelper.getSqliteHelper(App.getApp()).getWritableDatabase();
        saveCurrency(db, currency);
    }

    @NonNull
    public List<Currency> getCurrencies(String exchangedDate) {
        ArrayList<Currency> currencyArrayList = new ArrayList<>();
        Cursor cursor = null;
        try {
            SQLiteDatabase db = AppSqliteDbHelper.getSqliteHelper(App.getApp()).getReadableDatabase();
            cursor = db.query(TableNames.Currency.TABLE_NAME, null,
                    TableNames.Currency.EXCHANGE_DATE + " = ?", new String[]{exchangedDate},
                    null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                int idIndex = cursor.getColumnIndex(TableNames.Currency.ID);
                int r030Index = cursor.getColumnIndex(TableNames.Currency.R030);
                int nameIndex = cursor.getColumnIndex(TableNames.Currency.NAME);
                int rateIndex = cursor.getColumnIndex(TableNames.Currency.RATE);
                int currencyCodeIndex = cursor.getColumnIndex(TableNames.Currency.CURRENCY_CODE);
                int exchangeDateIndex = cursor.getColumnIndex(TableNames.Currency.EXCHANGE_DATE);
                do {
                    Currency currency = new Currency();
                    currency.setId(cursor.getInt(idIndex));
                    currency.setR030(cursor.getInt(r030Index));
                    currency.setName(cursor.getString(nameIndex));
                    currency.setRate(cursor.getDouble(rateIndex));
                    currency.setCurrencyCode(cursor.getString(currencyCodeIndex));
                    currency.setExchangeDate(cursor.getString(exchangeDateIndex));

                    currencyArrayList.add(currency);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            DebugLogger.e(CurrencyDbHelper.class.getSimpleName(), "getCurrencies", e);
        } finally {
            AppSqliteDbHelper.closeCursor(cursor);
        }
        return currencyArrayList;
    }

    @Override
    protected ContentValues toValues(@NonNull Currency currency) {
        ContentValues values = new ContentValues();
        if (currency.getId() > 0) {
            values.put(TableNames.Currency.ID, currency.getId());
        }
        values.put(TableNames.Currency.R030, currency.getR030());
        values.put(TableNames.Currency.NAME, currency.getName());
        values.put(TableNames.Currency.RATE, currency.getRate());
        values.put(TableNames.Currency.CURRENCY_CODE, currency.getCurrencyCode());
        values.put(TableNames.Currency.EXCHANGE_DATE, currency.getExchangeDate());
        return values;
    }

    @Override
    protected Currency fromCursor(@Nullable Cursor cursor) {
        if (cursor != null && cursor.moveToFirst()) {
            Currency currency = new Currency();
            currency.setId(cursor.getInt(cursor.getColumnIndex(TableNames.Currency.ID)));
            currency.setName(cursor.getString(cursor.getColumnIndex(TableNames.Currency.NAME)));
            currency.setRate(cursor.getFloat(cursor.getColumnIndex(TableNames.Currency.RATE)));
            currency.setCurrencyCode(cursor.getString(cursor.getColumnIndex(TableNames.Currency.CURRENCY_CODE)));
            currency.setExchangeDate(cursor.getString(cursor.getColumnIndex(TableNames.Currency.EXCHANGE_DATE)));
            currency.setR030(cursor.getInt(cursor.getColumnIndex(TableNames.Currency.R030)));
            return currency;
        }
        return null;
    }

    @NonNull
    @Override
    protected List<Currency> fromListCursor(@Nullable Cursor cursor) {
        return new ArrayList<>();
    }

    @NonNull
    @Override
    protected String getTableName() {
        return TableNames.Currency.TABLE_NAME;
    }

    @NonNull
    @Override
    protected String getIdColumnName() {
        return TableNames.Currency.ID;
    }

}
