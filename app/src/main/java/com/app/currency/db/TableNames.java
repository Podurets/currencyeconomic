package com.app.currency.db;

public abstract class TableNames {

    public static final String ID = "id";

    public interface Currency {
        String TABLE_NAME = "CurrencyTable";
        String ID = TableNames.ID;
        String R030 = "r030";
        String NAME = "name";
        String RATE = "rate";
        String CURRENCY_CODE = "cc";
        String EXCHANGE_DATE = "exchdate";
    }

    public interface EventNote {
        String TABLE_NAME = EventNote.class.getSimpleName();
        String ID = TableNames.ID;
        String TEXT = "note_text";
        String DESCRIPTION = "description";
        String DATE = "event_date";
        String NOTIFICATION_DATE = "notif_date";
    }
}
